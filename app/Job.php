<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    public function customer(){
    	return $this->belongsTo(User::class,'customer_id');
    }

    public function tailor(){
    	return $this->belongsTo(User::class,'tailor_id');
    }

    public function assigner(){
    	return $this->belongsTo(User::class,'assigned_by_id');
    }

    public function style(){
    	return $this->belongsTo(Style::class);
    }

    public function status(){
    	return $this->belongsTo(Status::class);
    }
}
