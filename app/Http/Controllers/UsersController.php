<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = \App\Group::lists('name','id');
        $groups = ['' => '---Select a group---'] + $groups->all();
        return view('users.create',compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $group = \App\Group::where('name','Clients')->first();


        $user = new \App\User;
        $user->date_registered = date('Y-m-d');
        $user->hear_us_from = \Request::get('hear_us_from');
        $user->first_name = \Request::get('first_name');
        $user->last_name = \Request::get('last_name');
        $user->group_id = \Request::get('group_id');
        $user->email = \Request::get('email');
        $user->address = \Request::get('address');
        $user->phone = \Request::get('phone');
        $user->gender = \Request::get('gender');
        $user->username = \Request::get('username');
        $user->password = \Hash::make(\Request::get('password'));
        $user->save();

        return redirect()->to('/admin/settings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $groups = \App\Group::lists('name','id');
        $groups = ['' => '---Select a group---'] + $groups->all();
        $user = \App\User::find($id);
        return view('users.edit',compact('groups','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = \App\User::find($id);
        $user->first_name = \Request::get('first_name');
        $user->last_name = \Request::get('last_name');
        $user->group_id = \Request::get('group_id');
        $user->email = \Request::get('email');
        $user->address = \Request::get('address');
        $user->phone = \Request::get('phone');
        $user->gender = \Request::get('gender');
        $user->save();

        return redirect()->to('/admin/settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = \App\User::find($id);
        $user->delete();
        return 'User Sucessfully deleted';
    }
}
