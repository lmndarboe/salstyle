<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tailor_group = \App\Group::where('name','Tailors')->first();
        $client_group = \App\Group::where('name','Clients')->first();
        $all_tailors = $tailor_group->users;
        $all_clients = $client_group->users;
        $clients = ['' => '---Select a customer---'];
        $tailors = ['' => '---Select a tailor---'];
        $other_users = \App\User::where('group_id','!=',$client_group->id)->get();
        $assigners = ['' => '---Select an assigner---'];

        foreach ($other_users as $user) {
            $assigners[$user->id] = $user->getName();
        }

        foreach ($all_clients as $client) {
            $clients[$client->id] = $client->getName();
        }

        foreach ($all_tailors as $tailor) {
            $tailors[$tailor->id] = $tailor->getName();
        }

        $styles = \App\Style::lists('name','id');
        $styles = ['' => '---Select a style---'] + $styles->all();
        $statuses = \App\Status::lists('name','id');
        $statuses = ['' => '---Select a status---'] + $statuses->all();
        
        return view('jobs.create',compact('tailors','clients','styles','statuses','assigners'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $job = new \App\Job;
        $job->customer_id = $request->get('customer_id');
        $job->style_id = $request->get('style_id');
        $job->tailor_id = $request->get('tailor_id');
        $job->assigned_by_id = $request->get('assigned_by_id');
        $job->date_assigned = $request->get('date_assigned');
        $job->date_to_be_completed = $request->get('date_to_be_completed');
        $job->description = $request->get('description');
        $job->status_id = $request->get('status_id');
        $job->save();

        return redirect()->to('/admin/settings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = \App\Job::find($id);
        $tailor_group = \App\Group::where('name','Tailors')->first();
        $client_group = \App\Group::where('name','Clients')->first();
        $all_tailors = $tailor_group->users;
        $all_clients = $client_group->users;
        $clients = ['' => '---Select a customer---'];
        $tailors = ['' => '---Select a tailor---'];
        $other_users = \App\User::where('group_id','!=',$client_group->id)->get();
        $assigners = ['' => '---Select an assigner---'];

        foreach ($other_users as $user) {
            $assigners[$user->id] = $user->getName();
        }

        foreach ($all_clients as $client) {
            $clients[$client->id] = $client->getName();
        }

        foreach ($all_tailors as $tailor) {
            $tailors[$tailor->id] = $tailor->getName();
        }

        $styles = \App\Style::lists('name','id');
        $styles = ['' => '---Select a style---'] + $styles->all();
        $statuses = \App\Status::lists('name','id');
        $statuses = ['' => '---Select a status---'] + $statuses->all();
        
        return view('jobs.edit',compact('tailors','clients','styles','statuses','assigners','job'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $job = \App\Job::find($id);
        $job->customer_id = $request->get('customer_id');
        $job->style_id = $request->get('style_id');
        $job->tailor_id = $request->get('tailor_id');
        $job->assigned_by_id = $request->get('assigned_by_id');
        $job->date_assigned = $request->get('date_assigned');
        $job->date_to_be_completed = $request->get('date_to_be_completed');
        $job->description = $request->get('description');
        $job->status_id = $request->get('status_id');
        $job->save();

        return redirect()->to('/admin/settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job = \App\Job::find($id);
        $job->delete();
        return 'Job Sucessfully deleted';
    }
}
