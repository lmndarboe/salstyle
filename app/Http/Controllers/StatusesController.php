<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class StatusesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('statuses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $sms = (isset($input['enabled_sms']) and $input['enabled_sms'] == "on") ? 'YES' : 'NO';
        $email = (isset($input['enabled_email']) and $input['enabled_email'] == "on") ? 'YES' : 'NO';
        
        $status = new \App\Status;
        $status->name = $request->get('name');
        $status->enabled_sms = $sms;
        $status->enabled_email = $email;
        $status->save();

        return redirect()->to('/admin/settings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = \App\Status::find($id);
        return view('statuses.edit',compact('status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $sms = (isset($input['enabled_sms']) and $input['enabled_sms'] == "on") ? 'YES' : 'NO';
        $email = (isset($input['enabled_email']) and $input['enabled_email'] == "on") ? 'YES' : 'NO';
        
        $status = \App\Status::find($id);
        $status->name = $request->get('name');
        $status->enabled_sms = $sms;
        $status->enabled_email = $email;
        $status->save();

        return redirect()->to('/admin/settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = \App\Status::find($id);
        $status->delete();

        return 'Status successfully deleted';
    }
}
