<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class StylesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clothings = \App\Clothing::lists('name','id');
        $clothings = ['' => '---Select a clothing use---'] + $clothings->all();
        return view('styles.create',compact('clothings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        // return $request->all();
        $image_path = "";
        if($request->hasFile('image')){
            
            $file = $request->file('image');
            $filename = str_random(20).'.'.$file->getClientOriginalExtension();
            $image_path = 'images/'.$filename;
            \Image::make($file->getRealPath())->resize(600,600,function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path().'/'.$image_path);

        }

        // return $image_path;

        $style = new \App\Style;
        $style->name = $request->get('name');
        $style->image = $image_path;
        $style->style_for = $request->get('style_for');
        $style->rank = $request->get('rank');
        $style->color = $request->get('color');
        $style->clothing_id = $request->get('clothing_id');
        $style->description = $request->get('description');
        $style->price = $request->get('price');
        $style->save();

        return redirect()->to('/admin/settings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $style = \App\Style::find($id);
        $clothings = \App\Clothing::lists('name','id');
        $clothings = ['' => '---Select a clothing use---'] + $clothings->all();
        return view('styles.edit',compact('clothings','style'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $style = \App\Style::find($id);
        $image_path = $style->image;
        if($request->hasFile('image')){
            $file = $request->file('image');
            $filename = str_random(20).'.'.$file->getClientOriginalExtension();
            $image_path = 'images/'.$filename;
            \Image::make($file->getRealPath())->resize(600,600,function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path().'/'.$image_path);

        }


        
        $style->name = $request->get('name');
        $style->image = $image_path;
        $style->style_for = $request->get('style_for');
        $style->rank = $request->get('rank');
        $style->color = $request->get('color');
        $style->clothing_id = $request->get('clothing_id');
        $style->description = $request->get('description');
        $style->price = $request->get('price');
        $style->save();

        return redirect()->to('/admin/settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $style = \App\Style::find($id);
        $style->delete();
        return 'Style Sucessfully deleted';
    }
}
