<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => 'auth'],function(){


	Route::get('/', function () {
		$styles = \App\Style::take(4)->get();
		$style_lists = \App\Style::distinct()->lists('style_for');
		return view('dash',compact('styles','style_lists'));
	});

	Route::get('/show-detail/{id}',function($id){
		$style = \App\Style::find($id);
		$related = \App\Style::where('style_for',$style->style_for)->orderBy('rank')->take(3)->get();
		return view('styles.style_detail',compact('style','related'));
	});

	Route::get('/load-more/',function(){
		$style_for = \Request::get('style_for');
		$styles = [];

		switch ($style_for) {
			case 'All':
			$styles = \App\Style::skip(4)->take(10000)->get();
			break;
			case 'Men':
			$styles = \App\Style::where('style_for','Men')->skip(4)->take(10000)->get();
			break;

			case 'Women':
			$styles = \App\Style::where('style_for','Women')->skip(4)->take(10000)->get();
			break;

			case 'Boy':
			$styles = \App\Style::where('style_for','Boy')->skip(4)->take(10000)->get();
			break;

			case 'Girl':
			$styles = \App\Style::where('style_for','Girl')->skip(4)->take(10000)->get();
			break;

			case 'Children':
			$styles = \App\Style::where('style_for','Children')->skip(4)->take(10000)->get();
			break;

			case 'Elders':
			$styles = \App\Style::where('style_for','Elders')->skip(4)->take(10000)->get();
			break;
			
			default:
			$styles = \App\Style::skip(4)->take(10000)->get();
			break;
		}
		
		return view('styles.load_more',compact('styles'));
	});

	Route::get('/gents/boys',function(){
		$styles = \App\Style::where('style_for','Boy')->take(4)->get();
		$style_lists = \App\Style::distinct()->lists('color');
		return view('styles.boys',compact('styles','style_lists'));
	});

	Route::get('/gents/men',function(){
		$styles = \App\Style::where('style_for','Men')->take(4)->get();
		$style_lists = \App\Style::distinct()->lists('color');
		return view('styles.men',compact('styles','style_lists'));

	});

Route::get('/ladies/girls', function() {
    $styles  = \App\Style::where('style_for', 'Girl')->take(4)->get();
    $style_lists = \App\Style::distinct()->lists('color');
    return view('styles.girls', compact('styles', 'style_lists'));
});

Route::get('ladies/women', function() {
    $styles = \App\Style::where('style_for', 'Women')->take(4)->get();
    $style_lists = \App\Style::distinct()->lists('color');
    return view('styles.women', compact('styles', 'style_lists'));
});

Route::get('/children', function() {
    $styles = \App\Style::where('style_for', 'children')->take(4)->get();
    $style_lists = \App\Style::distinct()->lists('color');
    return view('styles.children', compact('styles', 'style_lists'));
});

Route::get('/elders', function() {
    $styles = \App\Style::where('style_for', 'elders')->take(4)->get();
    $style_lists = \App\Style::distinct()->lists('color');
    return view('styles.elders', compact('styles', 'style_lists'));
});

});


Route::group(['prefix' => 'admin','middleware' => 'auth'],function(){
	
	Route::get('settings',function(){
		$users = \App\User::all();
		$tailor_group = \App\Group::where('name','Tailors')->first();
		$client_group = \App\Group::where('name','Clients')->first();
		$tailors = $tailor_group->users;
		$clients = $client_group->users;

		$clothings = \App\Clothing::all();
		$statuses = \App\Status::all();
		$styles = \App\Style::all();
		$jobs = \App\Job::all();

		return view('admin.settings',compact('users','tailors','clients','clothings','statuses','styles','jobs'));
	});

	Route::resource('users','UsersController');
	Route::resource('clients','ClientsController');
	Route::resource('tailors','TailorsController');
	Route::resource('styles','StylesController');
	Route::resource('clothings','ClothingsController');
	Route::resource('statuses','StatusesController');
	Route::resource('jobs','JobsController');
});

Route::post('/new-registration','RegistrationsController@store');



Route::auth();


