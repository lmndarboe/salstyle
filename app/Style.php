<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Style extends Model
{
    public function clothing(){
    	return $this->belongsTo(Clothing::class);
    }
}
