<div class="portfolio-content">
    <div class="cbp-l-project-title">{{ $style->name }}</div>
    <div class="cbp-l-project-subtitle">by Lamin Darboe</div>
    <div class="cbp-slider">
        <ul class="cbp-slider-wrap">
            <li class="cbp-slider-item">
                <a href="/{{ $style->image }}" class="cbp-lightbox">
                    <img src="/{{ $style->image }}" alt=""> </a>
            </li>
          
        </ul>
    </div>
    <div class="cbp-l-project-container">
        <div class="cbp-l-project-desc">
            <div class="cbp-l-project-desc-title">
                <span>Description</span>
            </div>
            <div class="cbp-l-project-desc-text">{{ $style->description }}</div>
        </div>
        <div class="cbp-l-project-details">
            <div class="cbp-l-project-details-title">
                <span>Details</span>
            </div>
            <ul class="cbp-l-project-details-list">
             <li>
                    <strong>Style For</strong> {{ $style->style_for }}</li>
                 <li>
                    <strong>Clothing Used</strong> {{ $style->clothing->name }} </li>
                <li>
                    <strong>Color</strong> {{ $style->color }} </li>
                <li>
                    <strong>Rank</strong> {{ $style->rank }}</li>
                <li>
                    <strong>Price</strong> GMD {{ number_format($style->price,2) }}</li>
            </ul>
            
        </div>
    </div>
    <div class="cbp-l-project-container">
        <div class="cbp-l-project-related">
            <div class="cbp-l-project-desc-title">
                <span>Related Styles</span>
            </div>
            <ul class="cbp-l-project-related-wrap">

                @foreach($related as $item)
                <li class="cbp-l-project-related-item">
                    <a href="/show-detail/{{$item->id }}" class="cbp-singlePage cbp-l-project-related-link" rel="nofollow">
                        <img src="/{{ $item->image }}" alt="">
                        <div class="cbp-l-project-related-title">{{ $item->name }}</div>
                    </a>
                </li>

                @endforeach
                
            </ul>
        </div>
    </div>
    <br>
    <br>
    <br> </div>