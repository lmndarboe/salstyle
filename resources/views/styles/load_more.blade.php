@foreach($styles->chunk(4) as $idx => $pages)

<div class="cbp-loadMore-block{{ ($idx+1) }}">
    
   
    @foreach($pages as $style)
            <div class="cbp-item {{ strtolower($style->style_for) }}">
                <div class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="/{{ $style->image }}" alt=""> </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <a href="/show-detail/{{$style->id}}" class="cbp-singlePage cbp-l-caption-buttonLeft btn red uppercase btn red uppercase" rel="nofollow">more info</a>
                                    <a href="/{{ $style->image}}" class="cbp-lightbox cbp-l-caption-buttonRight btn red uppercase btn red uppercase" data-title="Dashboard<br>by Paul Flavius Nechita">view larger</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cbp-l-grid-projects-title uppercase text-center uppercase text-center">{{ $style->name }}</div>
                    <div class="cbp-l-grid-projects-desc uppercase text-center uppercase text-center"> 

                    <h6>
                    <span class="price-sign" style="font-weight:strong;">GMD</span>{{ number_format($style->price,2) }}</h6>

                    </div>
                </div>

                @endforeach
     

</div>

   @endforeach







