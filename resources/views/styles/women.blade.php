@extends('layouts.master')



@section('page_title')

<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Styles For Women </h1>
        </div>
        <!-- END PAGE TITLE -->

    </div>
</div>

@stop


@section('content')






<div class="portfolio-content portfolio-1">
    <div id="js-filters-juicy-projects" class="cbp-l-filters-button">
        <div data-filter="*" class="cbp-filter-item-active cbp-filter-item btn dark btn-outline uppercase"> All
            <div class="cbp-filter-counter"></div>
        </div>

        @foreach($style_lists as $list)
        <div data-filter=".{{ strtolower($list) }}" class="cbp-filter-item btn dark btn-outline uppercase"> {{ $list }}
            <div class="cbp-filter-counter"></div>

        </div>

        @endforeach

    </div>
    <div id="js-grid-juicy-projects" class="cbp">

        @foreach($styles as $style)
        <div class="cbp-item {{ strtolower($style->color) }}">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <img src="/{{ $style->image }}" alt=""> </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <a href="/show-detail/{{$style->id}}" class="cbp-singlePage cbp-l-caption-buttonLeft btn red uppercase btn red uppercase" rel="nofollow">more info</a>
                            <a href="/{{$style->image}}" class="cbp-lightbox cbp-l-caption-buttonRight btn red uppercase btn red uppercase" data-title="Dashboard<br>by Paul Flavius Nechita">view larger</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cbp-l-grid-projects-title uppercase text-center uppercase text-center">{{ $style->name }}</div>
            <div class="cbp-l-grid-projects-desc uppercase text-center uppercase text-center">

                <h6><span class="badge badge-primary" style="font-weight:strong;">GMD {{ number_format($style->price,2) }}</span></h6>

            </div>
        </div>

        @endforeach




    </div>


    <div id="js-loadMore-juicy-projects" class="cbp-l-loadMore-button">
        <a href="/load-more?style_for=Men" class="cbp-l-loadMore-link btn grey-mint btn-outline" rel="nofollow">
            <span class="cbp-l-loadMore-defaultText">LOAD MORE</span>
            <span class="cbp-l-loadMore-loadingText">LOADING...</span>
            <span class="cbp-l-loadMore-noMoreLoading">NO MORE WORKS</span>
        </a>
    </div>






    @stop