@extends('layouts.master')

@section('content')

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-sharp">
            <i class="icon-speech font-green-sharp"></i>
            <span class="caption-subject bold uppercase"> New Style</span>

        </div>
        <div class="actions">

            <a href="/admin/settings" class="btn btn-circle btn-danger btn-sm">
                <i class="fa fa-arrow"></i> Back </a>

            </div>
        </div>
        <div class="portlet-body">


        
         
         {!! Form::open(['url' => '/admin/styles' , 'class' => 'register-form' ,'method' => 'POST','files' => true]) !!}
                <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                
                

                <div class="row">
                <div class="form-group col-md-6">
                    <label class="control-label visible-ie8 visible-ie9">Style Name</label>
                    <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                        {!! Form::text('name',null,['class' => 'form-control input-circle-right','required','placeholder' => 'Style Name']) !!}
                        
                        </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="control-label visible-ie8 visible-ie9">Style Image</label>
                    <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                    {!! Form::file('image',null,['class' => 'form-control input-circle-right','required','placeholder' => 'Style Image']) !!}
                    </div>
                        
                </div>


                </div>


             



                <div class="row">
                <div class="form-group col-md-6">
                   
                    <label class="control-label visible-ie8 visible-ie9">Style For</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                     {!! Form::select('style_for',
                    ['' => '---Select a style for---',
                    'Men' => 'Men',
                    'Women' => 'Women',
                    'Boy' => 'Boy',
                    'Girl' => 'Girl',
                    'Children' => 'Children',
                    'Elders' => 'Elders'
                    ]
                    ,null,['class' => 'select2 form-control','required']) !!}
                        </div>
                </div>

                 <div class="form-group col-md-6">
                   
                    <label class="control-label visible-ie8 visible-ie9">Rank</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                     {!! Form::select('rank',
                    ['' => '---Select a rank---',
                    '1' => '1',
                    '2' => '2']
                    ,null,['class' => 'select2 form-control','required']) !!}
                    </div>
                        
                </div>
                </div>



                   <div class="row">


                <div class="form-group col-md-6">
                   
                    <label class="control-label visible-ie8 visible-ie9">Color</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                     {!! Form::text('color',null,['class' => 'form-control input-circle-right','required','placeholder' => 'Color']) !!}
                        </div>
                </div>

                 <div class="form-group col-md-6">
                   
                    <label class="control-label visible-ie8 visible-ie9">Clothing Used</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                     {!! Form::select('clothing_id',
                    $clothings
                    ,null,['class' => 'select2 form-control','required']) !!}
                    </div>
                        
                </div>
                </div>







                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Description</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                    {!! Form::textarea('description',null,['class' => 'form-control input-circle-right','required','placeholder' => 'Description']) !!}
                    </div>
                        
                </div>




                    <div class="row">

                   
                <div class="form-group col-md-2">
                   
                    <label class="control-label visible-ie8 visible-ie9">Price</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                     {!! Form::text('price',null,['class' => 'form-control input-circle-right','required','placeholder' => 'Price']) !!}
                        </div>
                </div>

              
                </div>








               
                
                <div class="form-actions">
                    
                    <button type="submit" id="register-submit-btn" class="btn green pull-right"> New Style </button>
                </div>

                <div>
                    <br><br><br>
                </div>
            </form>




        </div>

</div>



            @stop