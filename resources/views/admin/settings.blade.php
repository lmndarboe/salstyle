    @extends('layouts.master')



    @section('page_title')

    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Administration </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
    </div>

    @stop


    @section('content')



    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE CONTENT BODY -->
    <div class="page-content">
        <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
        <div class="container">
            <!-- BEGIN PAGE BREADCRUMBS -->

            <!-- END PAGE BREADCRUMBS -->
            <!-- BEGIN PAGE CONTENT INNER -->
            <div class="page-content-inner">
                <div class="profile">
                    <div class="tabbable-line tabbable-full-width">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tailors" data-toggle="tab"> Tailors </a>
                            </li>

                            <li>
                                <a href="#clients" data-toggle="tab"> Clients </a>
                            </li>

                            <li>
                                <a href="#jobs" data-toggle="tab"> Jobs </a>
                            </li>

                            <li>
                                <a href="#styles" data-toggle="tab"> Styles </a>
                            </li>

                            <li>
                                <a href="#clothings" data-toggle="tab"> Clothings </a>
                            </li>

                            <li>
                                <a href="#statuses" data-toggle="tab"> Statuses </a>
                            </li>

                            <li>
                                <a href="#bills" data-toggle="tab"> Bills </a>
                            </li>

                            <li>
                                <a href="#users" data-toggle="tab"> Users </a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tailors">
                                <div class="row">

                                    <!-- BEGIN TAILORS -->
                                    <div class="col-md-12">

                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption font-green-sharp">
                                                    <i class="icon-speech font-green-sharp"></i>
                                                    <span class="caption-subject bold uppercase"> Tailors</span>

                                                </div>
                                                <div class="actions">

                                                    <a href="/admin/tailors/create" class="btn btn-circle btn-danger btn-sm">
                                                        <i class="fa fa-plus"></i> New Tailor </a>

                                                    </div>
                                                </div>
                                                <div class="portlet-body">

                                                    <div class="table-scrollable">
                                                        <table class="table table-striped table-bordered table-advance table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>
                                                                        <i class=""></i> Full Name
                                                                    </th>
                                                                    <th>
                                                                        <i class=""></i> Phone
                                                                    </th>

                                                                    <th>
                                                                        <i class=""></i> Address
                                                                    </th>


                                                                    <th>
                                                                        <i class=""></i> Actions
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($tailors as $user)
                                                                <tr>
                                                                    <td class="highlight">
                                                                        <div class="info"> </div>
                                                                        <a href="javascript:;"> {{ $user->getName() }} </a>
                                                                    </td>
                                                                    <td class="hidden-xs"> {{ $user->phone }} </td>

                                                                    <td class="hidden-xs"> {{ $user->address }} </td>




                                                                    <td>


                                                                        <a href="/admin/tailors/{{ $user->id }}/edit" class="btn btn-outline btn-circle green btn-sm purple">
                                                                            <i class="fa fa-edit"></i> Edit </a>

                                                                            <a href="/admin/tailors/{{ $user->id }}" class="btn btn-outline btn-circle dark btn-sm black delete-record">
                                                                                <i class="fa fa-trash-o"></i> Delete </a>

                                                                            </td>
                                                                        </tr>
                                                                        @endforeach

                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                        </div>
                                                    </div>


                                                </div>


                                                <!-- END TAILORS -->

                                            </div>


                                        </div>



                                        <div class="tab-pane" id="clients">
                                            <div class="row ">

                                                <!-- BEGIN CLIENTS -->


                                                <div class="portlet light bordered">
                                                    <div class="portlet-title">
                                                        <div class="caption font-green-sharp">
                                                            <i class="icon-speech font-green-sharp"></i>
                                                            <span class="caption-subject bold uppercase"> Clients</span>

                                                        </div>
                                                        <div class="actions">

                                                            <a href="/admin/clients/create" class="btn btn-circle btn-danger btn-sm">
                                                                <i class="fa fa-plus"></i> New Client </a>

                                                            </div>
                                                        </div>
                                                        <div class="portlet-body">

                                                            <div class="table-scrollable">
                                                                <table class="table table-striped table-bordered table-advance table-hover">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>
                                                                                <i class=""></i> Full Name
                                                                            </th>
                                                                            <th>
                                                                                <i class=""></i> Phone
                                                                            </th>

                                                                            <th>
                                                                                <i class=""></i> Address
                                                                            </th>


                                                                            <th>
                                                                                <i class=""></i> Actions
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @foreach($clients as $user)
                                                                        <tr>
                                                                            <td class="highlight">
                                                                                <div class="info"> </div>
                                                                                <a href="javascript:;"> {{ $user->getName() }} </a>
                                                                            </td>
                                                                            <td class="hidden-xs"> {{ $user->phone }} </td>

                                                                            <td class="hidden-xs"> {{ $user->address }} </td>




                                                                            <td>


                                                                                <a href="/admin/clients/{{ $user->id }}/edit" class="btn btn-outline btn-circle green btn-sm purple">
                                                                                    <i class="fa fa-edit"></i> Edit </a>

                                                                                    <a href="/admin/clients/{{ $user->id }}" class="btn btn-outline btn-circle dark btn-sm black delete-record">
                                                                                        <i class="fa fa-trash-o"></i> Delete </a>

                                                                                    </td>
                                                                                </tr>
                                                                                @endforeach

                                                                            </tbody>
                                                                        </table>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <!-- END CLIENTS -->

                                                        </div>
                                                    </div>
                                                    <!--end tab-pane-->

                                                    <div class="tab-pane" id="jobs">
                                                        <div class="row ">
                                                           <!-- BEGIN JOBS -->

                                                           <div class="portlet light bordered">
                                                            <div class="portlet-title">
                                                                <div class="caption font-green-sharp">
                                                                    <i class="icon-speech font-green-sharp"></i>
                                                                    <span class="caption-subject bold uppercase"> Jobs</span>

                                                                </div>
                                                                <div class="actions">

                                                                    <a href="/admin/jobs/create" class="btn btn-circle btn-danger btn-sm">
                                                                        <i class="fa fa-plus"></i> New Job </a>

                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body">

                                                                    <div class="table-scrollable">
                                                                        <table class="table table-striped table-bordered table-advance table-hover">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>
                                                                                        <i class=""></i> Date Assigned
                                                                                    </th>

                                                                                    <th>
                                                                                        <i class=""></i> Customer
                                                                                    </th>

                                                                                    <th>
                                                                                        <i class=""></i> Style Name
                                                                                    </th>
                                                                                    <th>
                                                                                        <i class=""></i> Tailor
                                                                                    </th>

                                                                                    

                                                                                    <th>
                                                                                        <i class=""></i> Assigned By
                                                                                    </th>


                                                                                    <th>
                                                                                        <i class=""></i> Status
                                                                                    </th>




                                                                                    <th>
                                                                                        <i class=""></i> Actions
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                @foreach($jobs as $job)
                                                                                <tr>

                                                                                <td class="hidden-xs"> {{ $job->date_assigned }} </td>
                                                                                <td class="hidden-xs"> {{ $job->customer->getName() }} </td>


                                                                                <td class="hidden-xs"> {{ $job->style->name }} </td>

                                                                                <td class="hidden-xs"> {{ $job->tailor->getName() }} </td>

                                                                                    
                                                                                    <td class="hidden-xs"> {{ $job->assigner->getName() }} </td>

                                                                                    <td class="hidden-xs"> {{ $job->status->name }} </td>




                                                                                    <td>

                                                                                        <a href="/admin/measurements/{{ $job->id }}/edit" class="btn btn-outline btn-circle green btn-sm purple">
                                                                                            <i class="fa fa-edit"></i> Update Measurement </a>

                                                                                        <a href="/admin/jobs/{{ $job->id }}/edit" class="btn btn-outline btn-circle green btn-sm purple">
                                                                                            <i class="fa fa-edit"></i> Edit </a>

                                                                                            <a href="/admin/jobs/{{ $job->id }}" class="btn btn-outline btn-circle dark btn-sm black delete-record">
                                                                                                <i class="fa fa-trash-o"></i> Delete </a>

                                                                                            </td>
                                                                                        </tr>
                                                                                        @endforeach

                                                                                    </tbody>
                                                                                </table>
                                                                            </div>

                                                                        </div>
                                                                    </div>


                                                           <!-- END JOBS -->

                                                       </div>
                                                   </div>
                                                   <!--end tab-pane-->


                                                   <div class="tab-pane" id="styles">
                                                    <div class="row ">


                                                        <!-- BEGIN STYLES -->




                                                        <div class="portlet light bordered">
                                                            <div class="portlet-title">
                                                                <div class="caption font-green-sharp">
                                                                    <i class="icon-speech font-green-sharp"></i>
                                                                    <span class="caption-subject bold uppercase"> Styles</span>

                                                                </div>
                                                                <div class="actions">

                                                                    <a href="/admin/styles/create" class="btn btn-circle btn-danger btn-sm">
                                                                        <i class="fa fa-plus"></i> New Style </a>

                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body">

                                                                    <div class="table-scrollable">
                                                                        <table class="table table-striped table-bordered table-advance table-hover">
                                                                            <thead>
                                                                                <tr>

                                                                                    <th>
                                                                                        <i class=""></i> Image
                                                                                    </th>

                                                                                    <th>
                                                                                        <i class=""></i> Style Name
                                                                                    </th>
                                                                                    <th>
                                                                                        <i class=""></i> Style For
                                                                                    </th>

                                                                                    <th>
                                                                                        <i class=""></i> Clothing Used
                                                                                    </th>

                                                                                    <th>
                                                                                        <i class=""></i> Color
                                                                                    </th>


                                                                                    <th>
                                                                                        <i class=""></i> Rank
                                                                                    </th>




                                                                                    <th>
                                                                                        <i class=""></i> Actions
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                @foreach($styles as $style)
                                                                                <tr>

                                                                                <td class="hidden-xs"> <img class="thumbnail" style="height:50px;" src="/{{ $style->image }}"> </td>
                                                                                <td class="hidden-xs"> {{ $style->name }} </td>


                                                                                <td class="hidden-xs"> {{ $style->style_for }} </td>

                                                                                <td class="hidden-xs"> {{ $style->clothing_id }} </td>

                                                                                    
                                                                                    <td class="hidden-xs"> {{ $style->color }} </td>

                                                                                    <td class="hidden-xs"> {{ $style->rank }} </td>




                                                                                    <td>


                                                                                        <a href="/admin/styles/{{ $style->id }}/edit" class="btn btn-outline btn-circle green btn-sm purple">
                                                                                            <i class="fa fa-edit"></i> Edit </a>

                                                                                            <a href="/admin/styles/{{ $style->id }}" class="btn btn-outline btn-circle dark btn-sm black delete-record">
                                                                                                <i class="fa fa-trash-o"></i> Delete </a>

                                                                                            </td>
                                                                                        </tr>
                                                                                        @endforeach

                                                                                    </tbody>
                                                                                </table>
                                                                            </div>

                                                                        </div>
                                                                    </div>


                                                                    <!-- END STYLES -->

                                                                </div>
                                                            </div>
                                                            <!--end tab-pane-->


                                                            <div class="tab-pane" id="clothings">
                                                                <div class="row ">

                                                                    <!-- BEGIN CLOTHINGS -->


                                                                    <div class="col-md-12">

                                                                        <div class="portlet light bordered">
                                                                            <div class="portlet-title">
                                                                                <div class="caption font-green-sharp">
                                                                                    <i class="icon-speech font-green-sharp"></i>
                                                                                    <span class="caption-subject bold uppercase"> Clothing</span>

                                                                                </div>
                                                                                <div class="actions">

                                                                                    <a href="/admin/clothings/create" class="btn btn-circle btn-danger btn-sm">
                                                                                        <i class="fa fa-plus"></i> New Clothing </a>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="portlet-body">

                                                                                    <div class="table-scrollable">
                                                                                        <table class="table table-striped table-bordered table-advance table-hover">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>
                                                                                                        <i class=""></i> Name
                                                                                                    </th>
                                                                                                    <th>
                                                                                                        <i class=""></i> Description
                                                                                                    </th>


                                                                                                    <th>
                                                                                                        <i class=""></i> Actions
                                                                                                    </th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                @foreach($clothings as $clothing)
                                                                                                <tr>
                                                                                                    <td class="highlight">
                                                                                                        <div class="info"> </div>
                                                                                                        <a href="javascript:;"> {{ $clothing->name }} </a>
                                                                                                    </td>
                                                                                                    <td class="hidden-xs"> {{ $clothing->description }} </td>






                                                                                                    <td>


                                                                                                        <a href="/admin/clothings/{{ $clothing->id }}/edit" class="btn btn-outline btn-circle green btn-sm purple">
                                                                                                            <i class="fa fa-edit"></i> Edit </a>

                                                                                                            <a href="/admin/clothings/{{ $clothing->id }}" class="btn btn-outline btn-circle dark btn-sm black delete-record">
                                                                                                                <i class="fa fa-trash-o"></i> Delete </a>

                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        @endforeach

                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>


                                                                                </div>

                                                                                <!-- END CLOTHINGS -->

                                                                            </div>
                                                                        </div>
                                                                        <!--end tab-pane-->


                                                                        <div class="tab-pane" id="statuses">
                                                                            <div class="row ">

                                                                             <!-- BEGIN STATUSES -->

                                                                             <div class="col-md-12">

                                                                                <div class="portlet light bordered">
                                                                                    <div class="portlet-title">
                                                                                        <div class="caption font-green-sharp">
                                                                                            <i class="icon-speech font-green-sharp"></i>
                                                                                            <span class="caption-subject bold uppercase"> Statuses</span>

                                                                                        </div>
                                                                                        <div class="actions">

                                                                                            <a href="/admin/statuses/create" class="btn btn-circle btn-danger btn-sm">
                                                                                                <i class="fa fa-plus"></i> New Status </a>

                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="portlet-body">

                                                                                            <div class="table-scrollable">
                                                                                                <table class="table table-striped table-bordered table-advance table-hover">
                                                                                                    <thead>
                                                                                                        <tr>
                                                                                                            <th>
                                                                                                                <i class=""></i> Status Name
                                                                                                            </th>
                                                                                                            <th>
                                                                                                                <i class=""></i> SMS Notification
                                                                                                            </th>

                                                                                                            <th>
                                                                                                                <i class=""></i> Email Notification
                                                                                                            </th>


                                                                                                            <th>
                                                                                                                <i class=""></i> Actions
                                                                                                            </th>
                                                                                                        </tr>
                                                                                                    </thead>
                                                                                                    <tbody>
                                                                                                        @foreach($statuses as $status)
                                                                                                        <tr>
                                                                                                            <td class="highlight">
                                                                                                                <div class="info"> </div>
                                                                                                                <a href="javascript:;"> {{ $status->name }} </a>
                                                                                                            </td>
                                                                                                            <td class="hidden-xs"> {{ $status->enabled_sms }} </td>

                                                                                                            <td class="hidden-xs"> {{ $status->enabled_email }} </td>




                                                                                                            <td>


                                                                                                                <a href="/admin/statuses/{{ $status->id }}/edit" class="btn btn-outline btn-circle green btn-sm purple">
                                                                                                                    <i class="fa fa-edit"></i> Edit </a>

                                                                                                                    <a href="/admin/statuses/{{ $status->id }}" class="btn btn-outline btn-circle dark btn-sm black delete-record">
                                                                                                                        <i class="fa fa-trash-o"></i> Delete </a>

                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                @endforeach

                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </div>

                                                                                                </div>
                                                                                            </div>


                                                                                        </div>

                                                                                        <!-- END STATUSES -->

                                                                                    </div>
                                                                                </div>
                                                                                <!--end tab-pane-->


                                                                                <div class="tab-pane" id="bills">
                                                                                    <div class="row ">

                                                                                        <!-- BEGIN BILLS -->

                                                                                        BILLS HERE

                                                                                        <!-- END BILLS -->

                                                                                    </div>
                                                                                </div>
                                                                                <!--end tab-pane-->


                                                                                <div class="tab-pane" id="users">
                                                                                    <div class="row ">


                                                                                        <!-- BEGIN USERS -->

                                                                                        <div class="col-md-12">


                                                                                            <div class="portlet light bordered">
                                                                                                <div class="portlet-title">
                                                                                                    <div class="caption font-green-sharp">
                                                                                                        <i class="icon-speech font-green-sharp"></i>
                                                                                                        <span class="caption-subject bold uppercase"> Users</span>

                                                                                                    </div>
                                                                                                    <div class="actions">

                                                                                                        <a href="/admin/users/create" class="btn btn-circle btn-danger btn-sm">
                                                                                                            <i class="fa fa-plus"></i> New User </a>

                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="portlet-body">

                                                                                                        <div class="table-scrollable">
                                                                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                                                                <thead>
                                                                                                                    <tr>
                                                                                                                        <th>
                                                                                                                            <i class=""></i> Full Name
                                                                                                                        </th>
                                                                                                                        <th>
                                                                                                                            <i class=""></i> Phone
                                                                                                                        </th>

                                                                                                                        <th>
                                                                                                                            <i class=""></i> Address
                                                                                                                        </th>

                                                                                                                        <th>
                                                                                                                            <i class=""></i> Group
                                                                                                                        </th>
                                                                                                                        <th>
                                                                                                                            <i class=""></i> Actions
                                                                                                                        </th>
                                                                                                                    </tr>
                                                                                                                </thead>
                                                                                                                <tbody>
                                                                                                                    @foreach($users as $user)
                                                                                                                    <tr>
                                                                                                                        <td class="highlight">
                                                                                                                            <div class="info"> </div>
                                                                                                                            <a href="javascript:;"> {{ $user->getName() }} </a>
                                                                                                                        </td>
                                                                                                                        <td class="hidden-xs"> {{ $user->phone }} </td>

                                                                                                                        <td class="hidden-xs"> {{ $user->address }} </td>

                                                                                                                        <td class="hidden-xs"> {{ $user->group->name }} </td>


                                                                                                                        <td>


                                                                                                                            <a href="/admin/users/{{ $user->id }}/edit" class="btn btn-outline btn-circle green btn-sm purple">
                                                                                                                                <i class="fa fa-edit"></i> Edit </a>

                                                                                                                                <a href="/admin/users/{{ $user->id }}" class="btn btn-outline btn-circle dark btn-sm black delete-record">
                                                                                                                                    <i class="fa fa-trash-o"></i> Delete </a>

                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            @endforeach

                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </div>

                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </div>


                                                                                                    <!-- END USERS -->

                                                                                                </div>
                                                                                            </div>
                                                                                            <!--end tab-pane-->






                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- END PAGE CONTENT INNER -->
                                                                        </div>
                                                                    </div>
                                                                    <!-- END PAGE CONTENT BODY -->
                                                                    <!-- END CONTENT BODY -->




                                                                    @stop