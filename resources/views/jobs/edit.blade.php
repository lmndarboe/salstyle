@extends('layouts.master')

@section('content')

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-sharp">
            <i class="icon-speech font-green-sharp"></i>
            <span class="caption-subject bold uppercase"> Update Job</span>

        </div>
        <div class="actions">

            <a href="/admin/settings" class="btn btn-circle btn-danger btn-sm">
                <i class="fa fa-arrow"></i> Back </a>

            </div>
        </div>
        <div class="portlet-body">


        
         
         {!! Form::model($job,['url' => '/admin/jobs/'.$job->id , 'class' => 'register-form' ,'method' => 'PUT']) !!}
                <input name="_token" type="hidden" value="{!! csrf_token() !!}" />

    
                



                <div class="row">


                <div class="form-group col-md-6">
                   
                    <label class="control-label visible-ie8 visible-ie9">Customer</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                     {!! Form::select('customer_id',$clients
                    ,null,['class' => 'form-control input-sm select2-multiple','required']) !!}
                    </div>
                        
                </div>

                <div class="form-group col-md-6">
                   
                    <label class="control-label visible-ie8 visible-ie9">Style</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                     {!! Form::select('style_id',
                    $styles
                    ,null,['class' => 'form-control input-sm select2-multiple','required',]) !!}
                    </div>
                        
                </div>


                </div>


             



                <div class="row">

                <div class="form-group col-md-6">
                   
                    <label class="control-label visible-ie8 visible-ie9">Tailor</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                     {!! Form::select('tailor_id',
                    $tailors
                    ,null,['class' => 'form-control input-sm select2-multiple','required',]) !!}
                    </div>
                        
                </div>

                 <div class="form-group col-md-6">
                   
                    <label class="control-label visible-ie8 visible-ie9">Assigned By</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                    {!! Form::select('assigned_by_id',
                    $assigners
                    ,null,['class' => 'form-control input-sm select2-multiple','required',]) !!}
                    </div>
                        
                </div>
                </div>



                   <div class="row">


                <div class="form-group col-md-6">
                   
                    <label class="control-label visible-ie8 visible-ie9">Date Assigned</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                     {!! Form::text('date_assigned',null,['class' => 'form-control input-circle-right','required','placeholder' => 'Date Assigned']) !!}
                        </div>
                </div>

                 <div class="form-group col-md-6">
                   
                    <label class="control-label visible-ie8 visible-ie9">Date To Be Completed</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                    {!! Form::text('date_to_be_completed',null,['class' => 'form-control input-circle-right','required','placeholder' => 'Date To Be Completed']) !!}
                    </div>
                        
                </div>
                </div>







                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Description</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                    {!! Form::textarea('description',null,['class' => 'form-control input-circle-right','required','placeholder' => 'Description']) !!}
                    </div>
                        
                </div>




                    <div class="row">

                   
                <div class="form-group col-md-3">
                   
                    <label class="control-label visible-ie8 visible-ie9">Status</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                     {!! Form::select('status_id',
                    $statuses
                    ,null,['class' => 'form-control input-sm select2-multiple','required',]) !!}
                        </div>
                </div>

              
                </div>








               
                
                <div class="form-actions">
                    
                    <button type="submit" id="register-submit-btn" class="btn green pull-right"> Update Job </button>
                </div>

                <div>
                    <br><br><br>
                </div>
            </form>




        </div>

</div>



            @stop


    