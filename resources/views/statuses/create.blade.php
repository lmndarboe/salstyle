@extends('layouts.master')

@section('content')
<style>
    input[type=checkbox] {
        zoom:1.4
    }
</style>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-sharp">
            <i class="icon-speech font-green-sharp"></i>
            <span class="caption-subject bold uppercase"> New Status</span>

        </div>
        <div class="actions">

            <a href="/admin/settings" class="btn btn-circle btn-danger btn-sm">
                <i class="fa fa-arrow"></i> Back </a>

            </div>
        </div>
        <div class="portlet-body">


        
         
         {!! Form::open(['url' => '/admin/statuses' , 'class' => 'register-form' ,'method' => 'POST']) !!}
                <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                
                

                <div class="row">
                <div class="form-group col-md-6">
                    <label class="control-label visible-ie8 visible-ie9">Status Name</label>
                    <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                        {!! Form::text('name',null,['class' => 'form-control input-circle-right','required','placeholder' => 'Status Name']) !!}
                        
                        </div>
                </div>

                </div>


                <div class="row">
                <div class="form-group col-md-4">
                   
                    <label class="control-label visible-ie8 visible-ie9">Description</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-check"></i>Enabled SMS
                    </span>
                    {!! Form::checkbox('enabled_sms',null,false,['class' => 'form-control input-circle-right','placeholder' => 'Description']) !!}
                        </div>
                </div>

                <div class="form-group col-md-4">
                   
                    <label class="control-label visible-ie8 visible-ie9">Description</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-check"></i>Enabled Email
                    </span>
                    {!! Form::checkbox('enabled_email',null,false,['class' => 'form-control input-circle-right','placeholder' => 'Description']) !!}
                        </div>
                </div>

                </div>



             
              
              
                </div>
           
                <div class="form-actions">
                    
                    <button type="submit" id="register-submit-btn" class="btn green pull-right"> Add Status </button>
                </div>

                <div>
                    <br><br><br>
                </div>
            </form>




        </div>

</div>



            @stop