@extends('layouts.master')

@section('content')

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-sharp">
            <i class="icon-speech font-green-sharp"></i>
            <span class="caption-subject bold uppercase"> New Tailor</span>

        </div>
        <div class="actions">

            <a href="/admin/settings" class="btn btn-circle btn-danger btn-sm">
                <i class="fa fa-arrow"></i> Back </a>

            </div>
        </div>
        <div class="portlet-body">


        
         
         {!! Form::open(['url' => '/admin/tailors' , 'class' => 'register-form' ,'method' => 'POST']) !!}
                <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                
                <legend>Personal Details</legend>

                <div class="row">
                <div class="form-group col-md-6">
                    <label class="control-label visible-ie8 visible-ie9">First Name</label>
                    <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                        {!! Form::text('first_name',null,['class' => 'form-control input-circle-right','required','placeholder' => 'First Name']) !!}
                        
                        </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="control-label visible-ie8 visible-ie9">Last Name</label>
                    <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                    {!! Form::text('last_name',null,['class' => 'form-control input-circle-right','required','placeholder' => 'Last Name']) !!}
                    </div>
                        
                </div>


                </div>


                <div class="row">
                <div class="form-group col-md-6">
                   
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                    {!! Form::text('email',null,['class' => 'form-control input-circle-right','required','placeholder' => 'Email']) !!}
                        </div>
                </div>

                 <div class="form-group col-md-6">
                   
                    <label class="control-label visible-ie8 visible-ie9">Phone</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                    {!! Form::text('phone',null,['class' => 'form-control input-circle-right','required','placeholder' => 'Phone']) !!}
                    </div>
                        
                </div>
                </div>



                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Address</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                    {!! Form::text('address',null,['class' => 'form-control input-circle-right','required','placeholder' => 'Address']) !!}
                    </div>
                        
                </div>
               
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Gender</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                    {!! Form::select('gender',
                    ['' => '---Select a gender---',
                    'Male' => 'Male',
                    'Female' => 'Female']
                    ,null,['class' => 'select2 form-control','required']) !!}
                   
                    </div>
                </div>

              
                
                <legend>Account Details</legend>

              


                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                     {!! Form::text('username',null,['class' => 'form-control input-circle-right','required','placeholder' => 'Username']) !!}

                        </div>
                </div>

                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                    {!! Form::text('password',null,['class' => 'form-control input-circle-right','required','placeholder' => 'Password','autocomplete' => 'off']) !!}

                        </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
                     <div class="input-group">
                    <span class="input-group-addon input-circle-left">
                        <i class="fa fa-font"></i>
                    </span>
                    {!! Form::text('password_confirmation',null,['class' => 'form-control input-circle-right','required','placeholder' => 'Re-type Your Password','autocomplete' => 'off']) !!}
                            

                            </div>
                    </div>
                </div>
           
                <div class="form-actions">
                    
                    <button type="submit" id="register-submit-btn" class="btn green pull-right"> New Tailor </button>
                </div>

                <div>
                    <br><br><br>
                </div>
            </form>




        </div>

</div>



            @stop