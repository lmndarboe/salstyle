<?php

use Illuminate\Database\Seeder;

class Init extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminGroup = \App\Group::create(['name' => 'Administrators']);
        \App\Group::create(['name' => 'Clients']);
        \App\Group::create(['name' => 'Tailors']);

        

        $user = new \App\User;
        $user->date_registered = date('Y-m-d');
        $user->first_name = "Super";
        $user->last_name = "Admin";
        $user->group_id = $adminGroup->id;
        $user->email = "admin@salstyle.com";
        $user->address = "Senegambia";
        $user->phone = "+2209331728";
        $user->gender = "Male";
        $user->username = "admin";
        $user->password = \Hash::make("123");
        $user->save();
    }
}
