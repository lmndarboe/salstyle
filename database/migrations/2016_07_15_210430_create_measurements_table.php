<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeasurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id')->unsigned();
            $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');
            $table->date('date');
            $table->decimal('height');
            $table->decimal('arm_length_elbow');
            $table->decimal('arm_length_full');
            $table->decimal('waist_size');
            $table->decimal('leg_length_knee');
            $table->decimal('leg_length_full');
            $table->decimal('shoulder_size');
            $table->decimal('neck_size');
            $table->decimal('stomach_size');
            $table->decimal('buttock_size');
            $table->string('taken_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('measurements');
    }
}
