<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStylesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('styles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clothing_id')->unsigned();
            $table->foreign('clothing_id')->references('id')->on('clothings')->onDelete('cascade');
            $table->string('name');
            $table->string('image');
            $table->enum('style_for',['Boy','Men','Girl','Women','Children','Elders']);
            $table->enum('rank',['1','2','3','4','5']);
            $table->text('description');
            $table->string('color');
            $table->decimal('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('styles');
    }
}
