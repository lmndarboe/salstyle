$(document).ready(function(){


	 // tab reloading

   $('a[data-toggle="tab"]').on('click', function (e) {
    localStorage.setItem('lastTab', $(e.target).attr('href'));
  });

   var lastTab = localStorage.getItem('lastTab');

   if (lastTab) {
    $('a[href="'+lastTab+'"]').click();
  }

  // end tab reloading


  
	

	$('table').on('click','a.delete-record',function(ev){
    ev.preventDefault();

    $route = $(this).attr('href');
    bootbox.confirm("<h3>Are you sure you want to Delete ?</h3>", function(result) {
     if(result){

       $.ajax({
        type: "POST",
        url : $route,
        data: {'_method':'DELETE'},
        beforeSend: function(request) {
          return request.setRequestHeader("X-CSRF-Token", $("input[name='_token']").val());
        },
        success: function(data){
          bootbox.alert("<h3 style='color:red;'>"+ data +" </h3>",function(result) {
            window.location.reload();
          });



        }
      });


     }
   }); 
  });

});